import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#0f0f0f',
  },
  imageContainer: {
    flex: 0.55,
    // backgroundColor: 'green',
    justifyContent: 'center',
    
  },
  detailsContainer: {
    flex: 0.10,
    // backgroundColor:'yellow',
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  controlsContainer: {
    flex: 0.15,
    // backgroundColor:'blue',
    justifyContent: 'flex-start',
  },
  buttonContainer: {
    flex: 0.10,
    // backgroundColor: 'red',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  albumImage: {
    width: 250,
    height: 250,
    alignSelf: 'center',
    borderRadius: 125,
  },
  progressBar: {
    height: 20,
    paddingBottom: 90,
  },
  songTitle: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  artist: {
    color: '#fefefe',
    fontSize: 14,
  },
  playButton: {
    backgroundColor: "#ffaa00",
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    padding: 11,
    margin: 10,
    borderRadius: 30
  },
  skipButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    padding: 11,
    margin: 10,
    borderRadius: 30
  }
});

export default styles;
