import React, {useEffect, useState} from 'react';
import {Text, Button, View, Image, TouchableOpacity} from 'react-native';
import TrackPlayer, {
  TrackPlayerEvents,
  STATE_PLAYING,
  STATE_READY,
  STATE_BUFFERING
} from 'react-native-track-player';
import {
  useTrackPlayerProgress,
  useTrackPlayerEvents,
} from 'react-native-track-player/lib/hooks';
import Slider from '@react-native-community/slider';
import styles from './styles';

const listOfSong = [ 
  {
    id: '25',
    url: require('./tracks/satu.mp3'),
    title: 'Yaa Sayyidi Yaa Rasulallah',
    album: 'Great Album',
    artist: 'Habib Hanif Al Attas',
    artwork: require('./thumb/habib-hanif.jpg'),
  },
  {
    id: '999',
    url: require('./tracks/dua.mp3'),
    title: 'Mabruk Alfa Mabruk',
    album: 'Great Album',
    artist: 'Habib Syech bin Abdul Qodir Assegaf',
    artwork: require('./thumb/habib-syech.jpg'),
  },
  {
    id: '324',
    url: require('./tracks/tiga.mp3'),
    title: 'Padhang Bulan, Sluku - Sluku Bathok',
    album: 'Great Album',
    artist: 'Habib Ali Zaenal Abidin Assegaf (Az Zahir)',
    artwork: require('./thumb/habib-bidin.jpg'),
  },
  {
    id: '564',
    url: require('./tracks/empat.mp3'),
    title: 'Lir - Ilir, Sholawat',
    album: 'Great Album',
    artist: 'Emha Ainun Najib ft. Kiai Kanjeng',
    artwork: require('./thumb/cak-nun.jpg'),
  },
]

const events = [
  TrackPlayerEvents.PLAYBACK_STATE,
  TrackPlayerEvents.PLAYBACK_ERROR
];

const trackPlayerInit = async () => {
  await TrackPlayer.setupPlayer();
  TrackPlayer.updateOptions({
    stopWithApp: true,
    capabilities: [
      TrackPlayer.CAPABILITY_PLAY,
      TrackPlayer.CAPABILITY_PAUSE,
      TrackPlayer.CAPABILITY_JUMP_FORWARD,
      TrackPlayer.CAPABILITY_JUMP_BACKWARD,
      TrackPlayer.CAPABILITY_SKIP,
      TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
      TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS
    ],
  });
  
  await TrackPlayer.add(listOfSong);

  return true;
};

const App = () => {
  const [isTrackPlayerInit, setIsTrackPlayerInit] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [sliderValue, setSliderValue] = useState(0);
  const [isSeeking, setIsSeeking] = useState(false);
  const [songData, setSongData ] = useState({id: '', url: '', title: '', album: '', artist: '', artwork: ''})
  const {position, duration} = useTrackPlayerProgress(250);

  useEffect(() => {
  const startPlayer = async () => {
     let isInit =  await trackPlayerInit();
     setIsTrackPlayerInit(isInit);
  }
  startPlayer();
}, []);

//when buffering
 useEffect(() => {
   if (!isSeeking && position && duration) {
     setSliderValue(position / duration);
   }
   getSongData();
 }, [position, duration]);

  useTrackPlayerEvents(events, event => {
    if (event.state === STATE_PLAYING) {
      setIsPlaying(true);
    } else if(event.state == 8 || event.state == 6 || event.state == 3){
      getSongData();
    } else {
      setIsPlaying(false);
    }

    console.log(event, "ini event")

  });

  const getSongData = async () => {
    const songIndex = await Promise.all([
      TrackPlayer.getCurrentTrack()
    ])
    listOfSong.find(e=>{
      if(e.id == songIndex){
        setSongData({
            id: e.id,
            url: e.url,
            title: e.title,
            album: e.album,
            artist: e.artist,
            artwork: e.artwork
        })
      }
    })

    // console.log(songData.artist, "testtteekjhkhjhk")
  }


  const onButtonPressed = () => {
    if (!isPlaying) {
      TrackPlayer.play();
      //setIsPlaying(true);
    } else {
      TrackPlayer.pause();
      //setIsPlaying(false);
    }
    
  };

  const nextSongButtonPressed = () =>{
    const songNowIndex = listOfSong.map(function(e) { return e.id; }).indexOf(songData.id); // get index of current play song in list of song
    if(songNowIndex < listOfSong.length - 1){
      TrackPlayer.skipToNext();
      console.log(songNowIndex, listOfSong.length, "terpanggil")
    }
  }

  const previousSongButtonPressed = () =>{
    const songNowIndex = listOfSong.map(function(e) { return e.id; }).indexOf(songData.id); // get index of current play song in list of song
    if((songNowIndex > 0) == true ){
      TrackPlayer.skipToPrevious();
    }
  }

  const slidingStarted = () => {
    setIsSeeking(true);
  };

  const slidingCompleted = async value => {
    await TrackPlayer.seekTo(value * duration);
    setSliderValue(value);
    setIsSeeking(false);
  };

  return (
    <View style={styles.mainContainer}>
      <View style={styles.imageContainer}>
        <Image
          source={songData.artwork}
          resizeMode="contain"
          style={styles.albumImage}
        />
      </View>
      <View style={styles.detailsContainer}>
        <Text style={styles.songTitle}>{songData.title}</Text>
        <Text style={styles.artist}>{songData.artist}</Text>
      </View>
      <View style={styles.controlsContainer}>
        <Slider
          style={styles.progressBar}
          minimumValue={0}
          maximumValue={1}
          value={sliderValue}
          minimumTrackTintColor="#fff111"
          maximumTrackTintColor="#f6f6f6"
          onSlidingStart={slidingStarted}
          onSlidingComplete={slidingCompleted}
          thumbTintColor="#fff"
        />
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity 
          style={styles.skipButton}
          onPress={previousSongButtonPressed}
        >
          <Image
            source={require('./img/6w.png')}
            style={{
              resizeMode: 'contain',
              height: 20,
              width: 20
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.playButton} 
          onPress={onButtonPressed} 
          disabled={!isTrackPlayerInit}
        >
          <Image
            source={isPlaying?require('./img/2w.png'):require('./img/3w.png')}
            style={{
              resizeMode: 'contain',
              height: 20,
              width: 20
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.skipButton} 
          onPress={nextSongButtonPressed}
        >
          <Image
            source={require('./img/7w.png')}
            style={{
              resizeMode: 'contain',
              height: 20,
              width: 20
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;